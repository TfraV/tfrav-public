package cloud.cave;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.testcontainers.containers.GenericContainer;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNotNull;

public class TestWallmessageMicroServiceCDT
{
    private String sAddress;
    private int nPort;

    @Rule
    public GenericContainer clQuoteContainer = new GenericContainer("tjuul7/message-service").withExposedPorts(4567);

    @Before
    public void setUp()
    {
        //Get the necessary info from the Test Container
        sAddress = clQuoteContainer.getContainerIpAddress();
        nPort = clQuoteContainer.getFirstMappedPort();
    }

    public HttpResponse<String> CreateHttpRequest( String sUrl, String sJsonInput )
    {
        try
        {
            HttpRequest clRequest = HttpRequest.newBuilder()
                    .uri(URI.create("http://" + sAddress + ":" + nPort + sUrl))
                    .header("Content-type", "application/json")
                    .POST(HttpRequest.BodyPublishers.ofString(sJsonInput))
                    .build();

            return HttpClient.newHttpClient().send(clRequest, HttpResponse.BodyHandlers.ofString());
        }
        catch (IOException | InterruptedException e)
        {
            System.out.println(e.getMessage());
            assertNotNull( null );
        }

        return null;
    }

    @Test
    public void TestAddNewMessageWithCorrectPathShallReturnHttpCreated()
    {
        String sJson = "{ \"creatorId\" : \"mikkel_aarskort\", \"creatorName\" : \"Mikkel\", \"contents\" : \"Completely new message\"}";
        HttpResponse<String> clResponse = CreateHttpRequest( "/msdo/v1/message/(0,0,0)", sJson );

        //Expect that the HTTP GET command was OK
        assertThat(clResponse.statusCode(), is(HttpServletResponse.SC_CREATED));
    }

    @Test
    public void TestAddNewMessageWithIncorrectPathShallReturnHttpNotFound()
    {
        String sJson = "{ \"creatorId\" : \"mikkel_aarskort\", \"creatorName\" : \"Mikkel\", \"contents\" : \"Completely new message\"}";
        HttpResponse<String> clResponse = CreateHttpRequest( "/msdo/v1/wrongUrl/(0,0,0)", sJson );

        //Expect that the HTTP GET command was Not Found
        assertThat(clResponse.statusCode(), is(HttpServletResponse.SC_NOT_FOUND));
    }

    @Test
    public void TestAddNewMessageWithCorrectPathShallReturnCorrectResult()
    {
        String sJson = "{ \"creatorId\" : \"mikkel_aarskort\", \"creatorName\" : \"Mikkel\", \"contents\" : \"Completely new message\"}";
        HttpResponse<String> clResponse = CreateHttpRequest( "/msdo/v1/message/(0,0,0)", sJson );

        //Get the Json from the body
        JsonObject clJsonObject = JsonParser.parseString(clResponse.body()).getAsJsonObject();

        //Verify that the correct key values are present.
        assertNotNull(clJsonObject.get("creatorTimeStampISO8601"));
        assertThat(clJsonObject.get("creatorId").getAsString(), is("mikkel_aarskort"));
        assertThat(clJsonObject.get("creatorName").getAsString(), is("Mikkel"));
        assertThat(clJsonObject.get("contents").getAsString(), is("Completely new message"));
        assertNotNull(clJsonObject.get("id"));
    }

    @Test
    public void TestUpdateMessageWithCorrectPathAndFormatShallReturnHttpOK()
    {
        //First add a new messages
        String sJson = "{ \"creatorId\" : \"mikkel_aarskort\", \"creatorName\" : \"Mikkel\", \"contents\" : \"Updated message\"}";
        HttpResponse<String> clResponse = CreateHttpRequest( "/msdo/v1/message/(0,0,0)", sJson );

        //Get the ID from the JSon
        String sMessageId = JsonParser.parseString(clResponse.body()).getAsJsonObject().get("id").getAsString();

        //Create new string in Json format with an updated message
        String sUpdatedJson = "{ \"creatorId\" : \"mikkel_aarskort\", \"creatorName\" : \"Mikkel\", \"contents\" : \"Updated message\"}";
        clResponse = CreateHttpRequest( "/msdo/v1/message/(0,0,0)/" + sMessageId, sJson );

        //Expect that the HTTP GET command was OK
        assertThat(clResponse.statusCode(), is(HttpServletResponse.SC_OK));
    }

    @Test
    public void TestUpdateMessageWithIncorrectPathAndFormatShallReturnHttpNotFound()
    {
        //Create new string in Json format with an updated message
        String sUpdatedJson = "{ \"creatorId\" : \"mikkel_aarskort\", \"creatorName\" : \"Mikkel\", \"contents\" : \"Updated message\"}";
        HttpResponse<String> clResponse = CreateHttpRequest( "/msdo/v1/message/(0,0,0)/InCorrectMessageId", sUpdatedJson );

        //Expect that the HTTP GET command was Not Found
        assertThat(clResponse.statusCode(), is(HttpServletResponse.SC_NOT_FOUND));
    }

    @Test
    public void TestUpdateMessageWithCorrectPathAndFormatShallReturnHttpCorrectResult()
    {
        //First add a new messages
        String sJson = "{ \"creatorId\" : \"mikkel_aarskort\", \"creatorName\" : \"Mikkel\", \"contents\" : \"Updated message\"}";
        HttpResponse<String> clResponse = CreateHttpRequest( "/msdo/v1/message/(0,0,0)", sJson );

        //Get the ID from the JSon
        String sMessageId = JsonParser.parseString(clResponse.body()).getAsJsonObject().get("id").getAsString();

        //Create new string in Json format with an updated message
        String sUpdatedJson = "{ \"creatorId\" : \"mikkel_aarskort\", \"creatorName\" : \"Mikkel\", \"contents\" : \"Updated message\"}";
        clResponse = CreateHttpRequest( "/msdo/v1/message/(0,0,0)/" + sMessageId, sJson );

        //Get the Json from the body
        JsonObject clJsonObject = JsonParser.parseString(clResponse.body()).getAsJsonObject();

        //Verify that the correct key values are present.
        assertNotNull(clJsonObject.get("creatorTimeStampISO8601"));
        assertThat(clJsonObject.get("creatorId").getAsString(), is("mikkel_aarskort"));
        assertThat(clJsonObject.get("creatorName").getAsString(), is("Mikkel"));
        assertThat(clJsonObject.get("contents").getAsString(), is("Updated message"));
        assertThat(clJsonObject.get("id").getAsString(), is(sMessageId)); //Verify that the ID is the same.
    }

    @Test
    public void TestGetMessageListWithCorrectPathWillReturnListOfWallMessages()
    {
        try
        {
            //Add two messages to the WallMessage Micro Service
            String sJson = "{ \"creatorId\" : \"mikkel_aarskort\", \"creatorName\" : \"Mikkel\", \"contents\" : \"Message 1\"}";
            HttpResponse<String> clResponse = CreateHttpRequest( "/msdo/v1/message/(0,0,32)", sJson );

            sJson = "{ \"creatorId\" : \"mikkel_aarskort\", \"creatorName\" : \"Mikkel\", \"contents\" : \"Message 2\"}";
            clResponse = CreateHttpRequest( "/msdo/v1/message/(0,0,32)", sJson );

            //Connect with correct address to get the Quote Header
            HttpRequest clRequest = HttpRequest.newBuilder(URI.create("http://" + sAddress + ":" + nPort + "/msdo/v1/message/(0,0,32)/0/2")).header("accept", "application/json").build();
            clResponse = HttpClient.newHttpClient().send(clRequest, HttpResponse.BodyHandlers.ofString());

            //Expect that the HTTP GET command was OK
            assertThat(clResponse.statusCode(), is(HttpServletResponse.SC_OK));

            JsonArray clJsonArray = JsonParser.parseString(clResponse.body()).getAsJsonArray();

            assertThat(clJsonArray.size(), is(2 ));
            assertThat(clJsonArray.get(0).getAsJsonObject().get("contents").getAsString(), is("Message 2"));
            assertThat(clJsonArray.get(1).getAsJsonObject().get("contents").getAsString(), is("Message 1"));
        }
        catch (IOException | InterruptedException e)
        {
            System.out.println(e.getMessage());
            assertNotNull( null );
        }
    }
}
